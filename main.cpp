

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <stdio.h>
#include <iostream>
#include <chrono>

#define IMAGE_HEIGHT 1536
#define IMAGE_WIDTH 2048
#define IMAGE_16b_SIZE IMAGE_WIDTH *IMAGE_HEIGHT * 2
#define IMAGE_8b_SIZE IMAGE_WIDTH *IMAGE_HEIGHT 
#define INITIALIZE_TIMER() \
  std::chrono::high_resolution_clock::time_point begin; \
  std::chrono::high_resolution_clock::time_point end;

#define TIME_IT(process, msg) \
begin = std::chrono::high_resolution_clock::now(); \
process; \
end = std::chrono::high_resolution_clock::now(); \
std::cout << msg << ": " << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << " [ms]" << std::endl;

void load_image(const std::string &filename, void *image) {
  FILE *file;
  // std::cout << "filename = " << filename << std::endl;
  file = fopen(filename.c_str(), "rb");
  size_t read_bytes = fread(image, 1, IMAGE_16b_SIZE, file);
  if (read_bytes != IMAGE_16b_SIZE) {
    printf("read_bytes < IMAGE_SIZE\n");
  }
  fclose(file);
}

void load_image(const std::string &filename, void *image, int size) {
  FILE *file;
  // std::cout << "filename = " << filename << std::endl;
  file = fopen(filename.c_str(), "rb");
  size_t read_bytes = fread(image, 1, size, file);
  if (read_bytes != size) {
    printf("read_bytes < IMAGE_SIZE\n");
  }
  fclose(file);
}

class TiledAlgorithm
{
    public:
    int mTileSize, mPadding, mBorderType;
    TiledAlgorithm(int tileSize, int padding, int borderType)
        : mTileSize(tileSize)
        , mPadding(padding)
        , mBorderType(borderType)
    {
    }

    void process(const cv::Mat& sourceImage, cv::Mat& resultImage) const
    {
        assert(!resultImage.empty());
        assert(sourceImage.rows == resultImage.rows);
        assert(sourceImage.cols == resultImage.cols);

        int rows = (sourceImage.rows / mTileSize) + (sourceImage.rows % mTileSize ? 1 : 0);
        int cols = (sourceImage.cols / mTileSize) + (sourceImage.cols % mTileSize ? 1 : 0);

        cv::Mat tileInput, tileOutput;

        for (int rowTile = 0; rowTile < rows; rowTile++)
        {
            for (int colTile = 0; colTile < cols; colTile++)
            {
                cv::Rect srcTile(colTile * mTileSize - mPadding, 
                                 rowTile * mTileSize - mPadding, 
                                 mTileSize + 2 * mPadding, 
                                 mTileSize + 2 * mPadding);

                cv::Rect dstTile(colTile * mTileSize,            
                                 rowTile * mTileSize, 
                                 mTileSize, 
                                 mTileSize);

                copySourceTile(sourceImage, tileInput, srcTile);
                processTileImpl(tileInput, tileOutput);
                // cv::imshow("bgr", tileOutput);
                // cv::waitKey(0);
                copyTileToResultImage(tileOutput, resultImage, dstTile);
            }
        }
    }

protected:
    virtual void processTileImpl(const cv::Mat& srcTile, cv::Mat& dstTile) const = 0;
    
    void copySourceTile(const cv::Mat& src, cv::Mat& srcTile, cv::Rect &tile) const;
    void copyTileToResultImage(const cv::Mat& tileImage, cv::Mat& resultImage, cv::Rect resultRoi) const;

};

void TiledAlgorithm::copySourceTile(const cv::Mat& src, cv::Mat& srcTile, cv::Rect &tile) const
{
    auto tl = tile.tl();
    auto br = tile.br();

    cv::Point tloffset, broffset;

    //Take care of border cases
    if (tile.x < 0)
    {
        tloffset.x = -tile.x;
        tile.x = 0;
    }

    if (tile.y < 0)
    {
        tloffset.y = -tile.y;
        tile.y = 0;
    }

    if (br.x >= src.cols)
    {
        broffset.x = br.x - src.cols + 1;
        tile.width -= broffset.x;
    }

    if (br.y >= src.rows)
    {
        broffset.y = br.y - src.rows + 1;
        tile.height -= broffset.y;
    }

    // If any of the tile sides exceed source image boundary we must use copyMakeBorder to make proper paddings for this side
    if (tloffset.x > 0 || tloffset.y > 0 || broffset.x > 0 || broffset.y > 0)
    {
        cv::Rect paddedTile(tile.tl(), tile.br());
        assert(paddedTile.x >= 0);
        assert(paddedTile.y >= 0);
        assert(paddedTile.br().x < src.cols);
        assert(paddedTile.br().y < src.rows);

        cv::copyMakeBorder(src(paddedTile), srcTile, tloffset.y, broffset.y, tloffset.x, broffset.x, mBorderType);
    }
    else
    {
        // Entire tile (with paddings lies inside image and it's safe to just take a region:
        src(tile).copyTo(srcTile);
    }
}

void TiledAlgorithm::copyTileToResultImage(const cv::Mat& tileImage, cv::Mat& resultImage, cv::Rect resultRoi) const
{
    cv::Rect srcTile(mPadding, mPadding, mTileSize, mTileSize);

    auto br = resultRoi.br();

    if (br.x >= resultImage.cols)
    {
        resultRoi.width -= br.x - resultImage.cols;
        srcTile.width -= br.x - resultImage.cols;
    }

    if (br.y >= resultImage.rows)
    {
        resultRoi.height -= br.y - resultImage.rows;
        srcTile.height -= br.y - resultImage.rows;
    }

    cv::Mat tileView = tileImage(srcTile);
    cv::Mat dstView = resultImage(resultRoi);

    assert(tileView.rows == dstView.rows);
    assert(tileView.cols == dstView.cols);

    tileView.copyTo(dstView);
}

class DebayerAlgorithm: public TiledAlgorithm
{
    public:
    DebayerAlgorithm(int tileSize, int padding, int borderType):TiledAlgorithm(tileSize, padding, borderType)
    {

    }
    protected:
    void processTileImpl(const cv::Mat& srcTile, cv::Mat& dstTile) const override;
};
void DebayerAlgorithm::processTileImpl(const cv::Mat& srcTile, cv::Mat& dstTile) const
{
    cv::Sobel(srcTile, dstTile, CV_8U, 1, 0);
}

int main(int argc, char *argv[]) {
    if (argc < 2)
    {
        printf("usage '%s IMAGE_GRAYSCALE_PATH'\n", argv[0]);
        return 0;
    }
    
    INITIALIZE_TIMER()
    auto alg = DebayerAlgorithm(256, 4, cv::BORDER_DEFAULT);
    cv::Mat input_image, out_image;
    input_image = cv::imread(argv[1], cv::IMREAD_GRAYSCALE);
    out_image = cv::Mat(input_image.size(), CV_8UC1);
    TIME_IT(alg.process(input_image, out_image);, "Tiled debayer")
    cv::imshow("sobel", out_image);
    cv::waitKey(0);
}
